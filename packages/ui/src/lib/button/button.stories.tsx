import { Story, Meta } from '@storybook/react';
import { Button, ButtonProps } from './button';

export default {
  component: Button,
  title: 'Button',
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = { backgroundColor: '#ff0', children: 'Default text' };

export const Secondary = Template.bind({});
Secondary.args = { ...Default.args, children: '😄👍😍💯' };

export const Tertiary = Template.bind({});
Tertiary.args = { ...Default.args, label: '📚📕📈🤓' };
