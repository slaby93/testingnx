import styled from 'styled-components';

export interface ButtonProps {
  className?: string;
  children: string;
  onClick?: () => void;
}

export function Button({ className, children, onClick }: ButtonProps) {
  return (
    <button className={className} onClick={onClick}>
      {children}
    </button>
  );
}

const Styled = styled(Button)`
`;

export default Styled;
